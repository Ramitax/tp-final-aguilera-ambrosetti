#ifndef _Student_List_H_
#define _Student_List_H_
#include "CourseList.h"

typedef struct Student {
    int id;
    char name[50];
    int years;
    AsignatureNode *asignatureNode;
} Student;

typedef struct StudentNode {
	Student *student;
	struct StudentNode *next;
} StudentNode;

Student* createStudent(char name[50], int years, int id);

StudentNode* createListStudent();

int lengthStudent(StudentNode *list);

Student* searchInPositionStudent(StudentNode *list, int position);

StudentNode *addElementStudent(StudentNode *list, Student* student);

Student* searchByNameStudent(StudentNode* list, char name[50]);

void addElementAsignatureInStudentByName(StudentNode* list, char name[50], Asignature* asignature);

StudentNode* searchByRangeYearsStudent(StudentNode *list, int lowerYear, int highYear);

StudentNode* deleteInPositionStudent(StudentNode *list, int position);

void printStudent(StudentNode *list);

StudentNode* sortedListStudent (StudentNode *list);

#endif // _Student_List_H_
