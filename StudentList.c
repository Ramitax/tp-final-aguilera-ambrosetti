#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "StudentList.h"
#include "CourseList.h"

Student* createStudent(char name[50], int years, int id){
	Student *student = malloc(sizeof(Student));
	student->id = id;
	strncpy(student->name, name, 50);
	student->years = years;
	student->asignatureNode = createListAsignature();
	return student;
}

StudentNode* createListStudent(){
	StudentNode *list = malloc(sizeof(StudentNode));
	list = NULL;
	return list;
}

int lengthStudent(StudentNode *list){
	if(list == NULL){
		return 0;
	} else {
			StudentNode *pointer = list;
			int value = 1;
			while(pointer->next != NULL){
				value = value + 1;
				pointer = pointer->next;
		}
		return value;
	}
}

Student* searchInPositionStudent(StudentNode *list, int position){
	if(position < lengthStudent(list)){
		StudentNode* pointer = list;
		int counter = 0;
		while(counter != position){
			pointer = pointer->next;
			counter = counter + 1;
		}
		return pointer->student;
	} else {
		return NULL;
	}
}

StudentNode *addElementStudent(StudentNode *list, Student* student){
	StudentNode *newNode = malloc(sizeof(StudentNode));
	newNode->student=student;
	newNode->next=NULL;
	if(list != NULL){
		StudentNode *pointer = list;
		while(pointer->next != NULL){
			pointer = pointer->next;
		}
		pointer->next = newNode;
	}
	else{
		list=newNode;
	}
	return list;
}

Student* searchByNameStudent(StudentNode* list, char name[50]){
	StudentNode* pointer = list;
	while(pointer->next != NULL) {
		if (strcmp(pointer->student->name, name) == 0) {
			return pointer->student;
		}
		pointer = pointer->next;
	}
	if (strcmp(pointer->student->name, name) == 0) {
			return pointer->student;
	}
	return NULL;
}

void updateApproveByNameStudent(StudentNode* list, char name[50], char name_aux[50]){
    StudentNode* pointer = list;
	while(pointer->next != NULL) {
		if (strcmp(pointer->student->name, name) == 0) {
			updateApproveByNameAsignature(pointer->student->asignatureNode, name_aux);
		}
		pointer = pointer->next;
	}
	if (strcmp(pointer->student->name, name) == 0) {
			updateApproveByNameAsignature(pointer->student->asignatureNode, name_aux);
	}
	return NULL;
}

void addElementAsignatureInStudentByName(StudentNode* list, char name[50], Asignature* asignature){
    Asignature* newAsignature = createAsignature(asignature->name,asignature->state);
	StudentNode* pointer = list;
	while(pointer->next != NULL) {
		if (strcmp(pointer->student->name, name) == 0) {
			pointer->student->asignatureNode = addElementAsignature(pointer->student->asignatureNode, newAsignature);
		}
		pointer = pointer->next;
	}
	if (strcmp(pointer->student->name, name) == 0) {
			pointer->student->asignatureNode = addElementAsignature(pointer->student->asignatureNode, newAsignature);
	}
	return NULL;
}

StudentNode* searchByRangeYearsStudent(StudentNode *list, int lowerYear, int highYear){
	StudentNode *pointer = list;
	StudentNode *newList = createListStudent();
	while (pointer->next != NULL) {
		if(pointer->student->years >= lowerYear && pointer->student->years <= highYear){
			Student* student = pointer->student;
			newList = addElementStudent(newList, student);
		}
		pointer = pointer->next;
	}
	if(pointer->student->years >= lowerYear && pointer->student->years <= highYear){
		Student* student = pointer->student;
		newList = addElementStudent(newList, student);
	}
	return newList;
}

StudentNode* deleteInPositionStudent(StudentNode *list, int position) {
	StudentNode *pointer = list;
	StudentNode *newList = createListStudent();
	int currentPosition = 0;
	while(pointer->next != NULL){
		if(currentPosition != position){
			Student *student = pointer->student;
			newList = addElementStudent(newList, student);
		}
		currentPosition = currentPosition + 1;
		pointer = pointer->next;
	}
	if(currentPosition != position){
		newList = addElementStudent(newList, pointer->student);
	}
	return newList;
}

void printStudent(StudentNode *list){
	StudentNode *pointer = list;
	int i;
    for (i = 0; i < lengthStudent(list) ; i++){
        printf("[%d] : \n     Id: %d \n     Nombre:%s \n     Edad: %d\n", i,pointer->student->id, pointer->student->name, pointer->student->years);
        printAsignature(pointer->student->asignatureNode);
        printf("\n");
        pointer = pointer->next;
    }
}

StudentNode* sortedListStudent(StudentNode *list){
    StudentNode* pointer = list;
    Student* lowerStudent = 0;
    Student* highStudent = 0;
    int large = lengthStudent(list);
    int y;
    for (y = 0; y < (large*2); y++){
    	int x;
        for (x = 0; x < (large-1); x++){
            if (pointer->student->years > pointer->next->student->years == 1){
                lowerStudent = pointer->next->student;
                highStudent = pointer->student;
                pointer->student = lowerStudent;
                pointer->next->student = highStudent;
            }else{
                pointer = pointer->next;
            }
        }
        pointer = list;
    }
    return list;
}
