#ifndef _Asignature_List_H_
#define _Asignature_List_H_
#include <stdbool.h>

typedef struct Asignature {
    char name[50];
    bool state;
} Asignature;

typedef struct AsignatureNode {
	Asignature *asignature;
	struct AsignatureNode *next;
} AsignatureNode;

Asignature* createAsignature(char name[50], bool state);

AsignatureNode* createListAsignature();

int lengthAsignature(AsignatureNode *list);

Asignature* searchInPositionAsignature(AsignatureNode *list, int position);

void updateApproveByNameAsignature(Asignature* list, char name[50]);

AsignatureNode *addElementAsignature(AsignatureNode *list, Asignature* asignature);

Asignature* searchByNameAsignature(AsignatureNode* list, char name[50]);

void* listedByNameAsignature(AsignatureNode* list, char name[50]);

AsignatureNode* deleteInPositionAsignature(AsignatureNode *list, int position);

void printAsignature(AsignatureNode *list);

#endif // _Asignature_List_H_

