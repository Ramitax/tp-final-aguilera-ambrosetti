
# Trabajo Practico final - Algoritmos y Programacion 3

En esta oportunidad desarrollamos una aplicacion para la gestion de alumnos y materias, los puntos que se abordaron fueron los siguientes:

- Dar de alta y listar estudiantes

- Buscar estudiantes por nombre

- Buscar estudiantes por rango de edad

- Dar de alta y listar materias

- Anotarse en una materia

- Rendir una materia

- Decision de no utilizar ordenamiento: Ya que el metodo de ordenamiento implementado tiene un alto coste algoritmico, haciendo que a medida que crezcan los registros crezcan los problemas de optimizacion.

- Las limitaciones del sistema: El sistema tiene un problema de optimizacion a la hora de manejar grandes cantidades de registros, ya que sus implementaciones son simples sin importar su coste algoritmico, por ejemplo: El addElement en cada una de las listas se hace recorriendo toda la lista y agregando al final, cuando tranquilamente se podria crear un puntero a la ultima posicion. Como esta hay otras optimizaciones, como eliminar de memoria los registros eliminados de la lista, etc.
## Autores

- [@Ramitax](https://gitlab.com/Ramitax)
- [@Leshitar](https://gitlab.com/agustin_aguilera)

  