#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "CourseList.h"


Asignature* createAsignature(char name[50], bool state){
	Asignature *asignature = malloc(sizeof(Asignature));
	strncpy(asignature->name, name, 50);
    asignature->state = state;
	return asignature;
}

AsignatureNode* createListAsignature(){
	AsignatureNode *list = malloc(sizeof(AsignatureNode));
	list = NULL;
	return list;
}

int lengthAsignature(AsignatureNode *list){
	if(list == NULL){
		return 0;
	} else {
			AsignatureNode *pointer = list;
			int value = 1;
			while(pointer->next != NULL){
				value = value + 1;
				pointer = pointer->next;
		}
		return value;
	}
}

Asignature* searchInPositionAsignature(AsignatureNode *list, int position){
	if(position < lengthAsignature(list)){
		AsignatureNode* pointer = list;
		int counter = 0;
		while(counter != position){
			pointer = pointer->next;
			counter = counter + 1;
		}
		return pointer->asignature;
	} else {
		return NULL;
	}
}

AsignatureNode *addElementAsignature(AsignatureNode *list, Asignature* asignature){
	AsignatureNode *newNode = malloc(sizeof(AsignatureNode));
	newNode->asignature=asignature;
	newNode->next=NULL;
	if(list != NULL){
		AsignatureNode *pointer = list;
		while(pointer->next != NULL){
			pointer = pointer->next;
		}
		pointer->next = newNode;
	}
	else{
		list=newNode;
	}
	return list;
}

Asignature* searchByNameAsignature(AsignatureNode* list, char name[50]){
	AsignatureNode* pointer = list;
	while(pointer->next != NULL) {
		if (strcmp(pointer->asignature->name, name) == 0) {
			return pointer->asignature;
		}
		pointer = pointer->next;
	}
	if (strcmp(pointer->asignature->name, name) == 0) {
			return pointer->asignature;
	}
	return NULL;
}

void updateApproveByNameAsignature(Asignature* list, char name[50]){
    AsignatureNode* pointer = list;
	while(pointer->next != NULL) {
		if (strcmp(pointer->asignature->name, name) == 0) {
			pointer->asignature->state = true;
		}
		pointer = pointer->next;
	}
	if (strcmp(pointer->asignature->name, name) == 0) {
			pointer->asignature->state = true;
	}
	return NULL;

}

AsignatureNode* deleteInPositionAsignature(AsignatureNode *list, int position) {
	AsignatureNode *pointer = list;
	AsignatureNode *newList = createListAsignature();
	int currentPosition = 0;
	while(pointer->next != NULL){
		if(currentPosition != position){
			Asignature *asignature = pointer->asignature;
			newList = addElementAsignature(newList, asignature);
		}
		currentPosition = currentPosition + 1;
		pointer = pointer->next;
	}
	if(currentPosition != position){
		newList = addElementAsignature(newList, pointer->asignature);
	}
	return newList;
}

void printAsignature(AsignatureNode *list){
	AsignatureNode *pointer = list;
	int i;
	printf("     Asignaturas: \n");
    for (i = 0; i < lengthAsignature(list) ; i++){
        printf("                 [%d]: %s \n", i,  pointer->asignature->name);
        printf("                      Estado: %s", pointer->asignature->state ? "Listado\n" : "No listado\n");
        pointer = pointer->next;
    }
}
