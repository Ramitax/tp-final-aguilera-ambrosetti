#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "CourseList.h"
#include "StudentList.h"
#include <stdbool.h>

int main (){
    int opcion, idState, id, years, lowYear, highYear;
    char temp[10], name[50], name_aux[50];
    bool state;
    AsignatureNode* listAsignature = createListAsignature();
    StudentNode *listStudent = createListStudent();
    StudentNode *newListStudent = createListStudent();
    while(opcion!=8){
        printf( "\n1. Dar de alta un estudiante");
        printf( "\n2. Dar de alta una materia");
        printf( "\n3. Buscar estudiante por nombre" );
        printf( "\n4. Buscar estudiantes por rango de edad" );
        printf( "\n5. Anotar un alumno en una materia");
        printf( "\n6. Rendir una materia");
        printf( "\n7. Imprimir informacion");
        printf( "\n8. Salir\n" );
        printf("Ingrese una opcion numerica entre el 1-8: ");
        fgets(temp, 10, stdin);
        opcion = atoi(temp);
        switch(opcion){
            case 1:
                printf("\nId del Alumno: ");
                scanf("%d", &id);
                printf("\nNombre del Alumno: ");
                scanf("%s", &name);
                printf("\nEdad del Alumno: ");
                scanf("%d", &years);
                Student* newStudent = createStudent(name, years, id);
                listStudent = addElementStudent(listStudent, newStudent);
                printf("\nAlumno listado correctamente\n");
                *name = "";
                id = -1;
                years = -1;
                break;
            case 2:
                printf("\nNombre de la materia: ");
                scanf("%s", &name);
                printf("\nEstado de la materia (0 - Aprobada 1 - No Aprobada): ");
                scanf("%d", &idState);
                if (idState == 0 ){
                    state = true;
                } else {
                    state = false;
                }
                Asignature* newAsignature = createAsignature(name, state);
                listAsignature = addElementAsignature(listAsignature, newAsignature);
                printf("\nMateria listada correctamente\n");
                *name = "";
                idState = -1;
                state = false;
                break;
            case 3:
                printf("\nNombre del Alumno: ");
                scanf("%s", &name);
                newListStudent =  addElementStudent(newListStudent, searchByNameStudent(listStudent, name));
                printf("/n");
                printStudent(newListStudent);
                newListStudent = createListStudent();
                break;
            case 4:
                printf("\nDesde: ");
                scanf("%d", &lowYear);
                printf("\Hasta: ");
                scanf("%d", &highYear);
                newListStudent = searchByRangeYearsStudent(listStudent, lowYear, highYear);
                printStudent(newListStudent);
                newListStudent = createListStudent();
                break;
            case 5:
                printf("\nNombre del Alumno: ");
                scanf("%s", &name);
                printf("\nNombre de la materia: ");
                scanf("%s", &name_aux);
                addElementAsignatureInStudentByName(listStudent, name, name_aux);
                *name = "";
                *name_aux = "";
                break;
            case 6:
                printf("\nNombre del Alumno: ");
                scanf("%s", &name);
                printf("\nNombre de la materia: ");
                scanf("%s", &name_aux);
                updateApproveByNameStudent(listStudent, name, name_aux);
                *name = "";
                *name_aux = "";
                break;
            case 7:
                printStudent(listStudent);
                break;
            case 8:
                printf("Saliendo del programa...");
                break;
            default:
                printf("Opcion incorrecta\n");
                break;
        }
    }
    return 0;
}
